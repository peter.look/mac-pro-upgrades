# mac-pro-upgrades

* [x] Pixlas Mod
* [x] Sapphire Nitro+ Radeon RX 580 w/ Apple EFI Boot
* [x] Update to NVMe boot drive
* [x] Upgrade to Mojave
* [x] Upgrade to Catalina
* [ ] ~~Bootcamp Windows 10~~
* [ ] ~~Install rEFInd & Ubuntu~~
* [ ] ~~Install Boot Manager/Next Loader~~
* [ ] ~~Install Ubuntu~~
* [x] Install OpenCore
* [x] Install LinuxMint
* [x] Install Steam Play
* [ ] Gigabyte Titan Ridge 2.0 card

## Pixlas Mod 
### References
- [Tutorial](https://thehouseofmoth.com/mac-pro-pixlas-mod/)
- [Analysis](https://thehouseofmoth.com/mac-pro-pixlas-mod-is-it-really-needed/)
- [Mod Origination](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652)
- [Clean OG Mod](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652/post-27334010)
- [Posi-Tap Mod](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652/post-24613080)
- [Board Connector Mod](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652/post-25650719)
- [Board Connector Mod w/Interconnect](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652/post-26670692)
- [Clean PSU Mod](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652/post-21722618)
- [PSU Mod w/Interconnect](https://forums.macrumors.com/threads/pixlas-4-1-mac-pro-mod.1859652/post-27947645)
- [GPU Guide](https://blog.greggant.com/posts/2018/05/07/definitive-mac-pro-upgrade-guide.html?utm_source=share&utm_medium=ios_app&utm_name=iossmf#gpuupgrades)
### Parts
- ~~[Posi-Taps](https://www.posi-products.com/posiplug.html) - chose tap 12-18 ga, accy 12-18 ga~~
- ~~[4-pin ATX connector extensions](https://www.amazon.com/gp/product/B01DV1Z36A/) - need 6 to re-arrange wiring to match~~
- [Braided Cable Sleeving](https://www.amazon.com/gp/product/B07F5G1RNF/)
- ~~[PCIe 8-pin Y-splitter](https://www.amazon.com/gp/product/B072JR4H3N/)~~
- ~~[PCIe 8-pin extension](https://www.amazon.com/gp/product/B07XQJ91QQ/)~~
- ~~[Molex pin extractor tool](https://www.amazon.com/gp/product/B00GOIY1NE/)~~
- [Open-End to Dual 8 Pin (6+2) PCI-E Sleeved Cable](https://www.moddiy.com/products/Open%252dEnd-to-Dual-8-Pin-%286%252b2%29-PCI%252dE-Sleeved-Cable-%2870cm-%252b-10cm%29.html)

## CPU Upgrade
### References
- https://www.youtube.com/watch?v=3sDZXpTP5ww
- https://www.youtube.com/watch?v=BXO8HtwFSww
- https://www.reddit.com/r/macpro/comments/exxs32/upgrading_51_cpu_x5680s/?utm_source=share&utm_medium=ios_app&utm_name=iossmf

## RAM Upgrade
### References
- https://www.reddit.com/r/macpro/comments/ey08pk/ram_config_51_10136/?utm_source=share&utm_medium=ios_app&utm_name=iossmf

## NVMe Boot Drive
### References
- [NVMe Drives for Mac Pro 5,1](https://www.reddit.com/r/macpro/comments/eximbx/nvme_ssd_adapter_for_512010/fgccy97?utm_source=share&utm_medium=web2x)
- [PCIe SSDs - NVMe & AHCI](https://forums.macrumors.com/threads/pcie-ssds-nvme-ahci.2146725/)
### Parts
- [Amfeltec 1U PCI Express Gen 3 Carrier Board for M.2 SSD modules](https://amfeltec.com/1u-pci-express-gen-3-carrier-board-for-m-2-ssd-module/)
- [Sonnet Technologies Tempo 6Gb/s SATA PCIe 2.0 Drive Card for Solid State Drives (TSATA6-SSD-E2)](https://www.amazon.com/gp/product/B0096P62G6 )
- [Sonnet M.2 4x4 PCIe Card](https://www.sonnetstore.com/products/sonnet-m2-4x4-pcie-card?variant=16605370449954)
- [MSI M.2 XPANDER-AERO](https://www.msi.com/PC-component/M.2-XPANDER-AERO)
- [PCIe SSDs - NVMe & AHCI](https://forums.macrumors.com/threads/pcie-ssds-nvme-ahci.2146725/)

## WiFi/BTLE Upgrade
### References
- https://www.crystalidea.com/blog/classic-mac-pro-wifi-bluetooth-upgrade
- [How to Fix Apple Mac Pro Bluetooth issue!](https://www.youtube.com/watch?v=d9xPvreK8tg)
- [OSXWiFi Handoff and Continuity Upgrade Kit](https://www.amazon.com/OSXWiFi-Mac-Pro-2009-2010-2012/dp/B07ZN754ZS/)
- [Apple Broadcom BCM943602CDP – 802.11 a/b/g/n/ac with Bluetooth 4.2 Upgrade Kit – Limited Edition](https://www.osxwifi.com/product/mac-pro-2009-41-and-mac-pro-2010-2012-51-apple-broadcom-bcm943602cdp-802-11-a-b-g-n-ac-with-bluetooth-4-2/)

## USB Cards
### References
- https://www.reddit.com/r/macpro/comments/7cczip/possible_to_power_usb_30_pcie_card_off_6pin_mini/?utm_source=share&utm_medium=ios_app&utm_name=iossmf
- [USB 3.x PCIe Cards for Classic Mac Pro](https://forums.macrumors.com/threads/usb-3-x-pcie-cards-for-classic-mac-pro.1501482/?utm_source=share&utm_medium=ios_app&utm_name=iossmf)
### Parts
- [NewerTech MAXPower 4-Port USB 3.1 Gen 1 PCIe Controller Card](https://eshop.macsales.com/item/NewerTech/MXPCIE4U3/)
- [Sonnet Allegro USB 3.0 PCIe 4-Port](https://www.amazon.com/Sonnet-Allegro-4-Port-Windows-Compatible/dp/B00GRGCV2G/)
- [Sonnet Allegro USB 3.2 PCIe 2-port](https://www.sonnettech.com/product/allegro-usbc-pcie.html) - Mac Pro 5,1 compatible!

## Thunderbolt 3
### References
- https://www.tonymacx86.com/threads/success-gigabyte-designare-z390-thunderbolt-3-i7-9700k-amd-rx-580.316533/page-1639?fbclid=IwAR3Q27041DBxFesciWG8mPohfG39sT_ax63lKrKkXKk-pNOvxxYHbIDWUNg#post-2087524
- https://forums.macrumors.com/threads/testing-tb3-aic-with-mp-5-1.2143042/page-27
- https://github.com/ameyrupji/thunderbolt-macpro-5-1/blob/master/GC-TitanRidge.md

## Bootcamp
### References
- https://www.crystalidea.com/blog/classic-mac-pro-and-windows
- https://forums.macrumors.com/threads/how-to-boot-camp-without-a-boot-screen.2114788/page-9#post-26689280
- https://support.apple.com/en-us/HT208123
- https://forums.macrumors.com/threads/boot-manager-a-boot-screen-alternative-in-the-status-bar-of-your-mac.2145374/
- https://apple.stackexchange.com/questions/375371/how-can-i-triple-boot-macos-catalina-ubuntu-18-04-and-windows-10
- https://www.techspot.com/guides/1760-find-your-windows-product-key/
- https://fossbytes.com/how-i-removed-bloatware-from-windows-10/
- https://tb.rg-adguard.net/public.php

## Linux
- https://itsfoss.com/steam-play/
- https://linuxnewbieguide.org/how-to-install-linux-on-a-macintosh-computer/
- https://enotacoes.wordpress.com/2016/07/15/installing-ubuntu-on-a-mac-pro-61-late-2013/
- https://www.makeuseof.com/tag/install-linux-macbook-pro/
- https://liam-on-linux.livejournal.com/79263.html
- https://linuxmint-installation-guide.readthedocs.io/en/latest/multiboot.html

## Utilities
- ~~[Continuity Activation Tool](https://github.com/dokterdok/Continuity-Activation-Tool)~~
- [Drive Dx](https://binaryfruit.com/drivedx)
- [Cinebench](https://www.maxon.net/en-us/products/cinebench-r20-overview/)
- [Unigine Benchmarks](https://benchmark.unigine.com/)
- ~~[Boot Manager](https://abdyfran.co/projects/boot-manager)~~
- ~~[Next Loader](https://abdyfran.co/projects/next-loader)~~
- ~~[QuickBoot](https://buttered-cat.com/product/quickboot/)~~
- [Macs Fan Control](https://www.crystalidea.com/macs-fan-control/download)
- [Geekbench](https://www.geekbench.com/)
- [Blackmagic Design Disk Speedtest](https://apps.apple.com/us/app/blackmagic-disk-speed-test/id425264550?mt=12)
- [iStat Menus](https://bjango.com/mac/istatmenus/)
- ~~[Brigadier](https://github.com/timsutton/brigadier)~~
- [Clover Configurator](https://mackie100projects.altervista.org)
- [Opencore Legacy Patcher](https://dortania.github.io/OpenCore-Legacy-Patcher/)

## Other
- [Mac Pro PSU Connector Pinout](https://forums.macrumors.com/threads/need-mac-pro-2009-2012-psu-pin-out.2122393/post-27599527)
- [CPU Tray Identifier](https://forums.macrumors.com/threads/switching-cpu-tray-from-mac-pro-2010-to-mac-pro-2012.2116440/)
- [The Definitive Classic Mac Pro (2006-2012) Upgrade Guide](http://blog.greggant.com/posts/2018/05/07/definitive-mac-pro-upgrade-guide.html?utm_source=share&utm_medium=ios_app&utm_name=iossmf)
- [Martin Lo's OpenCore upgrade](https://forums.macrumors.com/threads/activate-amd-hardware-acceleration.2180095/page-53?post=28255048#post-28255048)
- [OpenCore Linux install](https://forums.macrumors.com/threads/opencore-on-the-mac-pro.2207814/?post=27914713)
- [Orico 2.5 SSD SATA to 3.5 Drive Adapter](https://www.amazon.com/ORICO-Adapter-Mounting-Bracket-Interface/dp/B01LZWX6PD/ref=sr_1_1_sspa?crid=BTN64IJW0XF9&dchild=1&keywords=orico+2.5+to+3.5+hard+drive+adapter&qid=1586106704&sprefix=Rico%2Caps%2C211&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyR1ZIUEhQRk5KWTBNJmVuY3J5cHRlZElkPUExMDE4OTU3VkFQOFlHQkc1Q09OJmVuY3J5cHRlZEFkSWQ9QTA5MDA4NDZLV0czSFlFOEFQWEQmd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl)
- [Rename drives in boot picker](https://www.youtube.com/watch?v=bK7rOsiQljI)
- https://forums.macrumors.com/forums/mac-pro.1/
- https://forums.macrumors.com/threads/mp5-1-what-you-have-to-do-to-upgrade-to-mojave-bootrom-upgrade-instructions-thread.2142418/
- https://www.backblaze.com/blog/how-to-wipe-a-mac-hard-drive/
- https://www.driverscape.com/manufacturers/apple/laptops-desktops/macpro5%2C1/703
